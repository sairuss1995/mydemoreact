import React from 'react'
import {NavLink} from "react-router-dom";

export default function Header() {
  return (
          <header>
              <NavLink exact={true} to="/">Home</NavLink>
              <NavLink exact={true} to="/about">About</NavLink>
              <NavLink exact={true} to="/users">Users</NavLink>
        
      </header>
  );
}

