import React, { Component } from 'react'
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {Home,About,Users,} from "./pages"
import Header from "./components/header"
export default class App extends Component {
    render() {
        return(
            <Switch>
                <Header/>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/users">
              <Users />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        )
    }
}